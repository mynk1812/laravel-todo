<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Laravel - ToDo App - by MS</title>

        <!-- CSS And JavaScript -->
        <link rel="stylesheet" type="text/css" href="css/custom.css"> 
    </head>

    <body>
        <div class="container">
            <nav class="navbar navbar-default">
                <!-- Navbar Contents -->
            </nav>
        </div>

        @yield('content')
    </body>
</html>