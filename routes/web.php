<?php
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// TODO Tasks list 
Route::get('/', function () {
    $task_list = Task::orderBy('created_at', 'asc')->get();
    return view('tasks', [
        'tasks' => $task_list
    ]);
});

//Add New TODO
Route::post('/task', function (Request $request) {

    // basic validations
    $validator = Validator::make($request->all(), [
        'name' => 'required|max:500',
    ]);

    if ($validator->fails()) {
          return redirect('/')
          ->withInput()
          ->withErrors($validator);
    }

    // todo task creation
    $task = new Task;
    $task->name = $request->name;
    $task->save();
    return redirect('/');

});

//Delete A TODO
Route::delete('/task/{task}', function (Task $task) {
    $task->delete();

    return redirect('/');
});
